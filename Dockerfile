FROM ubuntu:18.04

RUN mkdir /config
COPY config/* /config/
COPY run.sh /run.sh

CMD /run.sh

