#!/bin/bash

cp /config/redis.conf /var/config/redis.conf
cp /config/environment.sh  /var/config/environment.sh
cp /config/credentials.py /var/config/credentials.py 
cp /config/nginx.conf /var/config/nginx.conf
cp /config/settings.py /var/config/settings.py
cp /config/SECRET_KEY /var/config/SECRET_KEY
# rm -f /var/config/credentials.py
# echo "" >> /var/config/credentials.py

#echo DATABASES=$DATABASES >> /var/config/credentials.py
#echo BROADCAST_WEBSOCKET_SECRET=$BROADCAST_WEBSOCKET_SECRET >> /var/config/credentials.py

chmod 0777 /var/config/redis/
chmod 0777 /var/config/memcached/
# chmod 0777 /var/config/postgresql/
